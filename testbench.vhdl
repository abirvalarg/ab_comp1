library ieee;
use ieee.std_logic_1164.all;
library abm0816;

entity testbench is
end;

architecture struct of testbench is
	constant reset_count_target_value: integer range 0 to 16#ffff# := 2#110100#;
	signal clock: std_logic := '0';
	signal write_request, nrst: boolean;
	signal address: std_logic_vector(15 downto 0);
	signal cpu_data_out, rom_data_out, cpu_data_in: std_logic_vector(7 downto 0);
	signal reset_count: integer range 0 to 16#ffff# := 0;
begin
	process(clock)
	begin
		if rising_edge(clock) then
			if reset_count < reset_count_target_value then
				reset_count <= reset_count + 1;
			end if;
		end if;
	end process;

	clock <= not clock after 1 us;
	
	cpu: entity abm0816.cpu
		port map(
			nrst => nrst,
			irq => false,
			clk => clock,
			data_in => cpu_data_in,
			data_out => cpu_data_out,
			address => address,
			write_request => write_request
		);
	
	nrst <= reset_count = reset_count_target_value;
	
	cpu_data_in <= rom_data_out when address(15 downto 12) = x"f" else x"00";
	
	rom: entity work.system_rom
		port map(
			clka => clock,
			addra => address(11 downto 0),
			douta => rom_data_out
		);
end;

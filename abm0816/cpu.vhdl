library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity cpu is
	port(
		nrst, irq: in boolean;
		clk: in std_logic;
		data_in: in std_logic_vector(7 downto 0);
		data_out: out std_logic_vector(7 downto 0);
		address: out std_logic_vector(15 downto 0);
		write_request: out boolean
	);
end;

architecture struct of cpu is
	type state_t is (state_init, state_normal, state_irq);
	type index_regs_t is array(0 to 1) of std_logic_vector(7 downto 0);
	type fetch_kind_t is (fetch_imm, fetch_zp, fetch_abs_x, fetch_abs_y,
		fetch_zp_index, fetch_abs, fetch_zp_x, fetch_zp_y);
	
	signal state: state_t := state_init;
	signal reg_a, reg_s, tmp, instruction, index: std_logic_vector(7 downto 0);
	signal index_regs: index_regs_t;
	signal reg_p, address_buffer: std_logic_vector(15 downto 0);
	signal address_shift: std_logic_vector(7 downto 0) := x"00";
	signal cycle, fetch_cycle: integer range 0 to 7 := 0;
	signal fetching, fetch_request, fetch_done, instruction_done: boolean;
	signal fetch_kind: fetch_kind_t;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if nrst then
				if fetching then
					fetch_cycle <= fetch_cycle + 1;
				else
					if fetch_done then
						fetch_cycle <= 0;
					end if;
					if instruction_done then
						cycle <= 0;
						state <= state_normal;
					else
						cycle <= cycle + 1;
					end if;
				end if;
			else
				cycle <= 0;
				fetch_cycle <= 0;
				state <= state_init;
			end if;
		end if;
	end process;

	process(clk)
	begin
		if falling_edge(clk) then
			if nrst then
				case state is
					when state_init =>
						case cycle is
							when 0 =>
								-- wait for clocks to get to known state
							when 1 =>
								address_shift <= x"00";
								address_buffer <= x"fffe";
							when 2 =>
								tmp <= data_in;
								address_buffer <= x"ffff";
							when 3 =>
								reg_p <= data_in & tmp;
							when others =>
								-- ignore
						end case;

					when state_normal =>
						if fetching then
							-- simply set the address
							case fetch_kind is
								when fetch_imm =>
									case fetch_cycle is
										when 0 =>
											reg_p <= reg_p + 1;
											address_buffer <= reg_p;
											address_shift <= x"00";
										when others =>
									end case;
								
								when fetch_abs =>
									case fetch_cycle is
										when 0 =>
											reg_p <= reg_p + 1;
											address_buffer <= reg_p;
											address_shift <= x"00";
										when 1 =>
											reg_p <= reg_p + 1;
											tmp <= data_in;
											address_buffer <= reg_p;
										when 2 =>
											address_buffer <= data_in & tmp;
										when others =>
									end case;

								when others =>
									-- TODO
							end case;
						else
							if cycle = 0 then
								-- fetch
								reg_p <= reg_p + 1;
								address_buffer <= reg_p;
								address_shift <= x"00";
							elsif cycle = 1 then
								-- fetch
								instruction <= data_in;
							else
								-- exec
								if instruction(7 downto 3) = "11000" then	-- LD
									case cycle is
										when 2 =>
											-- after fetch
											reg_a <= data_in;
										when others =>
									end case;
								elsif instruction(7 downto 3) = "11001" then	-- ST
									case cycle is
										when 2 =>
											-- after fetch
											data_out <= reg_a;
										when others =>
									end case;
								elsif instruction(7 downto 3) = "11010" then	-- JMP
									case cycle is
										when 2 =>
											-- after fetch
											reg_p <= address_buffer + (x"00" & address_shift);
										when others =>
									end case;
								end if;
							end if;
						end if;
					
					when others =>
						-- TODO
				end case;
			else
				address_buffer <= x"0000";
				address_shift <= x"00";
			end if;
		end if;
	end process;
	
	fetch_request <=
		(state = state_normal and instruction(7 downto 3) = "11000" and cycle = 2) or	-- LD
		(state = state_normal and instruction(7 downto 3) = "11001" and cycle = 2) or	-- ST
		(state = state_normal and instruction(7 downto 3) = "11010" and cycle = 2);	-- JMP

	fetch_done <=
		(state = state_normal and fetch_request and fetch_kind = fetch_imm and fetch_cycle = 1) or
		(state = state_normal and fetch_request and fetch_kind = fetch_abs and fetch_cycle = 3);

	instruction_done <=
		(state = state_init and cycle = 3) or
		(state = state_normal and instruction = "00000000" and cycle = 1) or	-- NOP
		(state = state_normal and instruction(7 downto 3) = "11000" and cycle = 2 and not fetching) or	-- LD
		(state = state_normal and instruction(7 downto 3) = "11001" and cycle = 3) or	-- ST
		(state = state_normal and instruction(7 downto 3) = "11010" and cycle = 2 and not fetching);	-- JMP

	index <= index_regs(to_integer(unsigned(instruction(0 downto 0))));

	fetching <= fetch_request and not fetch_done;
	address <= address_buffer + (x"00" & address_shift);
	
	write_request <=
		(state = state_normal and instruction(7 downto 3) = "11001" and cycle = 2 and not fetching);	-- ST
	
	with instruction(2 downto 0) select fetch_kind <=
		fetch_imm when "000",
		fetch_zp when "001",
		fetch_abs_x when "010",
		fetch_abs_y when "011",
		fetch_zp_index when "100",
		fetch_abs when "101",
		fetch_zp_x when "110",
		fetch_zp_y when "111";
end;

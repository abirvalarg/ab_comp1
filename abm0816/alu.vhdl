package alu_p is
	type alu_operation is (alu_adc, alu_sbc, alu_and, alu_or, alu_xor, alu_lsl, alu_lsr);
end;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use abm0816.alu_p.all;

entity alu is
	port(
		operation: in alu_operation;
		a, b: in std_logic_vector(7 downto 0);
		flag_c_in: in std_logic;
		result: out std_logic_vector(7 downto 0);
		flag_z, flag_c, flag_n, flag_v: out std_logic
	);
end;

architecture struct of alu is
	signal r: std_logic_vector(8 downto 0);
begin
	with operation select r <=
		('0' & a) + ('0' & b) + flag_c_in when alu_adc,
		('0' & a) - ('0' & b) - flag_c_in when alu_sbc,
		a and b when alu_and,
		a or b when alu_or,
		a xor b when alu_xor,
		a & "0" when alu_lsl,
		a(0) & "0" & a(7 downto 1) when alu_lsr;
	
	result <= r(7 downto 0);
	flag_z <= '1' when r(7 downto 0) = x"00" else '0';
	flag_c <= r(8);
	flag_n <= r(7);
	flag_v <= '1' when a(7) = b(7) and r(7) /= a(7) else '0';
end;

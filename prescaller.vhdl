library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity prescaller is
	generic(
		psc_value: std_logic_vector(7 downto 0)
	);
	port(
		nrst: in boolean;
		clk_in: in std_logic;
		clk_out: out std_logic
	);
end;

architecture struct of prescaller is
	signal counter: std_logic_vector(7 downto 0) := x"00";
	signal output: std_logic := '0';
begin
	process(nrst, clk_in, counter, output)
		variable next_counter: std_logic_vector(7 downto 0) := counter;
		variable next_output: std_logic := output;
	begin
		if nrst then
			if clk_in'event and clk_in = '1' then
				next_counter := next_counter + 1;
				if next_counter = psc_value then
					next_counter := x"00";
					next_output := not next_output;
				end if;
			end if;
		else
			next_counter := x"00";
			next_output := '0';
		end if;
		counter <= next_counter;
		output <= next_output;
	end process;

	clk_out <= clk_in when psc_value = x"00" else output;
end;

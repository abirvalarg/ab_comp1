library ieee;
use ieee.std_logic_1164.all;
library abm0816;

entity ab_comp1 is
	port(
		clk: in std_logic;
		nrst: in boolean;
		btns: in std_logic_vector(1 to 4);
		leds: out std_logic_vector(0 to 3)
	);
end;

architecture struct of ab_comp1 is
	type peripheral is (per_ram, per_leds, per_btns, per_rom, per_none);

	signal system_clk: std_logic;
	signal selected_per: peripheral;
	signal write_request: boolean;
	signal cpu_data_out, cpu_data_in, ram_data_out,
		rom_data_out: std_logic_vector(7 downto 0);
	signal leds_values: std_logic_vector(0 to 3) := "0000";
	signal address: std_logic_vector(15 downto 0);
	signal ram_wea: std_logic_vector(0 to 0);
	signal reset_counter: integer range 0 to 16#ffff# := 0;
begin
	process(clk, system_clk)
	begin
		if rising_edge(system_clk) then
			if nrst then
				if reset_counter = 16#ffff# then
					if selected_per = per_leds and write_request then
						leds_values <= cpu_data_out(3 downto 0);
					end if;
				else
					reset_counter <= reset_counter + 1;
				end if;
			end if;
		end if;
		
		if rising_edge(clk) then
			if nrst then
				if reset_counter < 16#ffff# then
					leds_values <= "0000";
				end if;
			else
				reset_counter <= 0;
			end if;
		end if;
	end process;

	cpu: entity abm0816.cpu
		port map(
			nrst => nrst and reset_counter = 16#ffff#,
			irq => false,
			clk => system_clk,
			data_in => cpu_data_in,
			address => address,
			data_out => cpu_data_out,
			write_request => write_request
		);
	selected_per <=
		per_ram when address(15 downto 12) = x"0" else
		per_leds when address = x"1000" else
		per_btns when address = x"1001" else
		per_rom when address(15 downto 12) = x"f" else
		per_none;
	with selected_per select cpu_data_in <=
		ram_data_out when per_ram,
		"0000" & leds_values when per_leds,
		"0000" & btns when per_btns,
		rom_data_out when per_rom,
		x"00" when per_none;

	system_ram: entity work.system_ram
		port map(
			clka => system_clk,
			wea => ram_wea,
			addra => address(11 downto 0),
			dina => cpu_data_out,
			douta => ram_data_out
		);
	ram_wea <= "1" when selected_per = per_ram and write_request else "0";
	
	system_rom: entity work.system_rom
		port map(
			clka => system_clk,
			addra => address(11 downto 0),
			douta => rom_data_out
		);

	system_psc: entity work.prescaller
		generic map(
			psc_value => x"ff"
		)
		port map(
			clk_in => clk,
			nrst => nrst,
			clk_out => system_clk
		);
	
	leds <= leds_values;
	-- leds <= address(3 downto 0);
	-- leds(0) <= not system_clk;
	-- leds(1 to 3) <= "000";
end;
